import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadImovelRoutingModule } from './cad-imovel-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadImovelRoutingModule
  ]
})
export class CadImovelModule { }
