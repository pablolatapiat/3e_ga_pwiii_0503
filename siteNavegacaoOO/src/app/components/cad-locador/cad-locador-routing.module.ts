import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadLocadorComponent } from './cad-locador.component';

const routes: Routes = [
  { path: "", component: CadLocadorComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadLocadorRoutingModule { }
