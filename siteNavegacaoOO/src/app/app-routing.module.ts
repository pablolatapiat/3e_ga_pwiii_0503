import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: "",pathMatch: "full",redirectTo: "home"},
  {path: "home", loadChildren: () => import("./components/home/home.module").then(m => m.HomeModule)},
  {path: "cadImovel", loadChildren: () => import("./components/cad-imovel/cad-imovel.module").then(m => m.CadImovelModule)},
  {path: "cadImobiliaria", loadChildren: () => import("./components/cad-imobiliaria/cad-imobiliaria.module").then(m => m.CadImobiliariaModule)},
  {path: "cadLocador", loadChildren: () => import("./components/cad-locador/cad-locador.module").then(m => m.CadLocadorModule)},
  {path: "cadProprietario", loadChildren: () => import("./components/cad-proprietario/cad-proprietario.module").then(m => m.CadProprietarioModule)},
  {path: "**",loadChildren: () => import("./components/page-not-found/page-not-found.module").then(m => m.PageNotFoundModule)}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
